import React, { useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { Redirect, useHistory } from 'react-router-dom';
import { ApiService } from '../../Services/ApiService';
import { AppState } from '../../Store';

export const NewBrand = () => {
  const api = new ApiService();

  const history = useHistory();
  const auth = useSelector((state: AppState) => state.auth);
  const [name, setName] = useState('');
  const [loading, setLoading] = useState(false);
  const [redirect, setRedirect] = useState('');

  const handleChangeName = (event: any) => {
    setName(event.target.value);
  };

  const handleSubmit = async (event: any) => {
    event.preventDefault();
    setLoading(true);
    await api.createBrand(name, auth.user?.token || '');
    setLoading(false);
    setRedirect('/brands');
  };

  const handleCancel = () => {
    history.push('/brands');
  };

  if (redirect !== '') {
    return <Redirect to={redirect}></Redirect>;
  }

  return (
    <Container>
      <Row>
        <Col>
          <Form onSubmit={handleSubmit}>
            <Form.Group controlId='formBasicName'>
              <Form.Label>Brand name</Form.Label>
              <Form.Control
                type='text'
                placeholder='Enter name'
                onChange={handleChangeName}
              />
              <Form.Text className='text-muted'></Form.Text>
            </Form.Group>

            <Button variant='primary' type='submit' disabled={loading}>
              Accept
            </Button>
            <Button variant='danger' disabled={loading} onClick={handleCancel}>
              Cancel
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};
