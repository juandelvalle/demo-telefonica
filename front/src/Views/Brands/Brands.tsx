import React from 'react';
import { Table } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { ApiService } from '../../Services/ApiService';
import { Brand } from '../../Services/Models';
import { AppState } from '../../Store';

import './Brands.css';

type HandlerAction = (id: string) => void;
type BrandRowProps = {
  brand: Brand;
  onEdit: HandlerAction;
  onDelete: HandlerAction;
};

const BrandRow = ({ brand, onEdit, onDelete }: BrandRowProps) => {
  const { _id = '', name = '' } = brand;
  return (
    <tr>
      <td>{_id}</td>
      <td>{name}</td>
      <td>
        <span
          className='action'
          onClick={() => {
            onEdit(_id);
          }}>
          Edit
        </span>
        <span
          className='action danger'
          onClick={() => {
            onDelete(_id);
          }}>
          Delete
        </span>
      </td>
    </tr>
  );
};

export const Brands = () => {
  const api = new ApiService();
  const auth = useSelector((state: AppState) => state.auth);
  const history = useHistory();

  const handleEditAction = (id: string) => {
    history.push(`/brands/edit/${id}`);
  };
  const handleDeleteAction = async (id: string) => {
    await api.deleteBrand(id, auth.user?.token || '');
    refetch();
  };

  const [{ data, loading, error }, refetch] = api.useGetBrands(
    auth.user?.token || ''
  );

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error!</p>;

  const { Data = [] } = data;
  return (
    <>
      <Link className='right' to='/brands/new'>
        Add New brand
      </Link>
      <Table striped bordered hover>
        <thead>
          <th>Id</th>
          <th>Name</th>
          <th>Actions</th>
        </thead>
        <tbody>
          {Data?.map((brand: Brand) => {
            return (
              <BrandRow
                brand={brand}
                onEdit={handleEditAction}
                onDelete={handleDeleteAction}></BrandRow>
            );
          })}
        </tbody>
      </Table>
    </>
  );
};
