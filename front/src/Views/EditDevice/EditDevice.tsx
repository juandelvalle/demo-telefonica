import React, { useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { Redirect, useHistory, useParams } from 'react-router-dom';
import { ApiService } from '../../Services/ApiService';
import { Brand } from '../../Services/Models';
import { AppState } from '../../Store';
import { getBrandNameFromId } from '../../Utils';

export const EditDevice = () => {
  const api = new ApiService();
  let { id } = useParams() as any;

  const history = useHistory();
  const auth = useSelector((state: AppState) => state.auth);
  const [name, setName] = useState('');
  const [brand, setBrand] = useState('');
  const [isLoading, setLoading] = useState(false);
  const [redirect, setRedirect] = useState('');
  const [brands, refetchBrands] = api.useGetBrands(auth.user?.token || '');

  const handleChangeName = (event: any) => {
    setName(event.target.value);
  };

  const handleChangeBrand = (event: any) => {
    setBrand(event.target.value);
  };

  const handleSubmit = async (event: any) => {
    event.preventDefault();
    setLoading(true);
    debugger;
    await api.updateDevice(
      {
        _id: id,
        name,
        brand: {
          name: getBrandNameFromId(brand, brands?.data?.Data || []),
          _id: brand,
        },
      },
      auth.user?.token || ''
    );
    setLoading(false);
    setRedirect('/devices');
  };

  const handleCancel = () => {
    history.push('/devices');
  };

  const [{ data, loading, error }, refetch] = api.useGetDevice(
    id,
    auth.user?.token || ''
  );

  useEffect(() => {
    if (data?.Data?.name) {
      setName(data.Data.name);
    }

    if (data?.Data?.brand) {
      setBrand(data.Data.brand._id);
    }
  }, [data]);

  if (redirect !== '') {
    return <Redirect to={redirect}></Redirect>;
  }

  return (
    <Container>
      <Row>
        <Col>
          <Form onSubmit={handleSubmit}>
            <Form.Group controlId='formBasicName'>
              <Form.Label>Device name</Form.Label>
              <Form.Control
                type='text'
                placeholder='Enter name'
                value={name}
                onChange={handleChangeName}
              />
              <Form.Text className='text-muted'></Form.Text>
            </Form.Group>
            <Form.Group controlId='formSelectBrand'>
              <Form.Label>Select Brand</Form.Label>
              <Form.Control
                as='select'
                onChange={handleChangeBrand}
                value={brand}>
                {brands?.data?.Data &&
                  brands.data.Data.map((brand: Brand) => (
                    <option value={brand._id} key={brand._id}>
                      {brand.name}
                    </option>
                  ))}
              </Form.Control>
            </Form.Group>
            <Button variant='primary' type='submit' disabled={isLoading}>
              Accept
            </Button>
            <Button
              variant='danger'
              disabled={isLoading}
              onClick={handleCancel}>
              Cancel
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};
