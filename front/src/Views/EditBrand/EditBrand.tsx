import React, { useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { Redirect, useHistory, useParams } from 'react-router-dom';
import { ApiService } from '../../Services/ApiService';
import { AppState } from '../../Store';

export const EditBrand = () => {
  const api = new ApiService();
  let { id } = useParams() as any;

  const history = useHistory();
  const auth = useSelector((state: AppState) => state.auth);
  const [name, setName] = useState('');
  const [isLoading, setLoading] = useState(false);
  const [redirect, setRedirect] = useState('');

  const handleChangeName = (event: any) => {
    setName(event.target.value);
  };

  const handleSubmit = async (event: any) => {
    event.preventDefault();
    setLoading(true);
    await api.updateBrand({ _id: id, name }, auth.user?.token || '');
    setLoading(false);
    setRedirect('/brands');
  };

  const handleCancel = () => {
    history.push('/brands');
  };

  const [{ data, loading, error }, refetch] = api.useGetBrand(
    id,
    auth.user?.token || ''
  );

  useEffect(() => {
    if (data?.Data?.name) setName(data.Data.name);
  }, [data]);

  if (redirect !== '') {
    return <Redirect to={redirect}></Redirect>;
  }

  return (
    <Container>
      <Row>
        <Col>
          <Form onSubmit={handleSubmit}>
            <Form.Group controlId='formBasicName'>
              <Form.Label>Brand name</Form.Label>
              <Form.Control
                type='text'
                placeholder='Enter name'
                value={name}
                onChange={handleChangeName}
              />
              <Form.Text className='text-muted'></Form.Text>
            </Form.Group>

            <Button variant='primary' type='submit' disabled={isLoading}>
              Accept
            </Button>
            <Button
              variant='danger'
              disabled={isLoading}
              onClick={handleCancel}>
              Cancel
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};
