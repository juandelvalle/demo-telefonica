import React, { useState } from 'react';
import { Container, Row, Col, Button, Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { AppState } from '../../Store';
import { AuthLoginAction } from '../../Store/Auth/Actions';

import './Login.css';

export const Login = () => {
  const auth = useSelector((state: AppState) => state.auth);
  const dispatch = useDispatch();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  if (auth.user) {
    localStorage.setItem('dtauth', auth.user.token || '');
    return <Redirect to='/' />;
  }

  // Handle Change email
  const handleChangeEmail = (event: any) => {
    setEmail(event.target.value);
  };

  // Handle Change password
  const handleChangePassword = (event: any) => {
    setPassword(event.target.value);
  };

  // Handle submit form
  const handleSubmit = (event: any) => {
    event.preventDefault();
    dispatch(AuthLoginAction({ email, password }));
  };

  return (
    <Container className='loginWrapper'>
      <Row>
        <Col className='loginCard'>
          <Form onSubmit={handleSubmit}>
            <Form.Group controlId='formBasicEmail'>
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type='email'
                placeholder='Enter email'
                onChange={handleChangeEmail}
              />
              <Form.Text className='text-muted'></Form.Text>
            </Form.Group>

            <Form.Group controlId='formBasicPassword'>
              <Form.Label>Password</Form.Label>
              <Form.Control
                type='password'
                placeholder='Password'
                onChange={handleChangePassword}
              />
            </Form.Group>
            <div className='error'>{auth.error}</div>
            <Button
              variant='primary'
              type='submit'
              disabled={auth.loggingInProgress}>
              Login
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};
