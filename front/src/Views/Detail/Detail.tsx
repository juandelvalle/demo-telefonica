import React from 'react';
import { Button, Col, Container, Row } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { ApiService } from '../../Services/ApiService';
import { AppState } from '../../Store';

export const Detail = () => {
  const api = new ApiService();
  let { id } = useParams() as any;

  const history = useHistory();
  const auth = useSelector((state: AppState) => state.auth);

  const handleBack = () => {
    history.push('/devices');
  };

  const [{ data, loading, error }, refetch] = api.useGetDevice(
    id,
    auth.user?.token || ''
  );

  if (loading) {
    return (
      <Container>
        <Row>
          <Col>Loading...</Col>
        </Row>
      </Container>
    );
  }

  const { Data } = data;

  return (
    <Container>
      <Row>
        <Col>Device Id: {Data._id}</Col>
      </Row>
      <Row>
        <Col>Device Name: {Data.name}</Col>
      </Row>
      <Row>
        <Col>Device Brand: {Data.brand.name}</Col>
      </Row>
      <Row>
        <Button variant='primary' onClick={handleBack}>
          Back
        </Button>
      </Row>
    </Container>
  );
};
