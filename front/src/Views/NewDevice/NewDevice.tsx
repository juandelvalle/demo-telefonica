import React, { useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { Redirect, useHistory } from 'react-router-dom';
import { ApiService } from '../../Services/ApiService';
import { Brand } from '../../Services/Models';
import { AppState } from '../../Store';
import { getBrandNameFromId } from '../../Utils';

export const NewDevice = () => {
  const api = new ApiService();

  const history = useHistory();
  const auth = useSelector((state: AppState) => state.auth);
  const [name, setName] = useState('');
  const [brand, setBrand] = useState('');
  const [redirect, setRedirect] = useState('');
  const [brands, refetch] = api.useGetBrands(auth.user?.token || '');

  const handleChangeName = (event: any) => {
    setName(event.target.value);
  };

  const handleChangeBrand = (event: any) => {
    setBrand(event.target.value);
  };

  const handleSubmit = async (event: any) => {
    event.preventDefault();
    let brandId = brand;
    if (brandId === '') {
      brandId = brands?.data?.Data[0]._id || '';
    }
    await api.createDevice(
      {
        name,
        brand: {
          name: getBrandNameFromId(brandId, brands?.data?.Data || []),
          _id: brandId,
        },
      },
      auth.user?.token || ''
    );
    setRedirect('/devices');
  };

  const handleCancel = () => {
    history.push('/devices');
  };

  if (redirect !== '') {
    return <Redirect to={redirect}></Redirect>;
  }

  return (
    <Container>
      <Row>
        <Col>
          <Form onSubmit={handleSubmit}>
            <Form.Group controlId='formBasicName'>
              <Form.Label>Device name</Form.Label>
              <Form.Control
                type='text'
                placeholder='Enter name'
                onChange={handleChangeName}
              />
              <Form.Text className='text-muted'></Form.Text>
            </Form.Group>

            <Form.Group controlId='formSelectBrand'>
              <Form.Label>Select Brand</Form.Label>
              <Form.Control as='select' onChange={handleChangeBrand}>
                {brands?.data?.Data &&
                  brands.data.Data.map((brand: Brand) => (
                    <option value={brand._id} key={brand._id}>
                      {brand.name}
                    </option>
                  ))}
              </Form.Control>
            </Form.Group>

            <Button variant='primary' type='submit'>
              Accept
            </Button>
            <Button variant='danger' onClick={handleCancel}>
              Cancel
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};
