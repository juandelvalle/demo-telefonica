import React from 'react';
import { Row } from 'react-bootstrap';
import Switch from 'react-bootstrap/esm/Switch';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import { NavBar } from '../../Components/NavBar';
import PrivateRoute from '../../Components/PrivateRoute';
import { TopBar } from '../../Components/TopBar/TopBar';
import { AppState } from '../../Store';
import { UserAuth } from '../../Store/Auth/Reducer';
import { AuthGetUser } from '../../Store/Auth/Selectors';
import { Brands } from '../Brands/Brands';
import { Dashboard } from '../Dashboard/Dashboard';
import { Detail } from '../Detail/Detail';
import { Devices } from '../Devices/Devices';
import { EditBrand } from '../EditBrand/EditBrand';
import { EditDevice } from '../EditDevice/EditDevice';
import { NewBrand } from '../NewBrand/NewBrand';
import { NewDevice } from '../NewDevice/NewDevice';

import './Home.css';
interface HomeProps extends RouteComponentProps {
  user: UserAuth | null;
}

interface HomeState {}
class _Home extends React.Component<HomeProps, HomeState> {
  render() {
    const { user, location } = this.props;
    const { pathname } = location;

    return (
      <div className='appWrapper'>
        <TopBar user={user}></TopBar>
        <NavBar user={user}></NavBar>
        <div className='appContent'>
          <Row>
            <h2>{pathname.substring(1).toLocaleUpperCase()}</h2>
          </Row>

          <Switch>
            <PrivateRoute path='/brands' exact component={Brands} />
            <PrivateRoute path='/brands/new' exact component={NewBrand} />
            <PrivateRoute path='/brands/edit/:id' exact component={EditBrand} />
            <PrivateRoute path='/devices' exact component={Devices} />
            <PrivateRoute path='/devices/new' exact component={NewDevice} />
            <PrivateRoute
              path='/devices/edit/:id'
              exact
              component={EditDevice}
            />
            <PrivateRoute
              path='/devices/details/:id'
              exact
              component={Detail}
            />
            <PrivateRoute path='/' exact component={Dashboard} />
          </Switch>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: AppState) => ({
  user: AuthGetUser(state.auth),
});

export const Home = connect(mapStateToProps)(withRouter(_Home));
