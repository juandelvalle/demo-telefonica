import React from 'react';
import { Table } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { ApiService } from '../../Services/ApiService';
import { Device } from '../../Services/Models';
import { AppState } from '../../Store';

import './Devices.css';

type HandlerAction = (id: string) => void;
type DeviceRowProps = {
  device: Device;
  onEdit: HandlerAction;
  onDelete: HandlerAction;
  onDetails: HandlerAction;
};

const DeviceRow = ({ device, onEdit, onDelete, onDetails }: DeviceRowProps) => {
  const { _id = '', name = '', brand } = device;
  return (
    <tr>
      <td>{_id}</td>
      <td>{name}</td>
      <td>{brand?.name}</td>
      <td>
        <span
          className='action'
          onClick={() => {
            onEdit(_id);
          }}>
          Edit
        </span>
        <span
          className='action'
          onClick={() => {
            onDetails(_id);
          }}>
          Details
        </span>
        <span
          className='action danger'
          onClick={() => {
            onDelete(_id);
          }}>
          Delete
        </span>
      </td>
    </tr>
  );
};

export const Devices = () => {
  const api = new ApiService();
  const auth = useSelector((state: AppState) => state.auth);
  const history = useHistory();

  const handleEditAction = (id: string) => {
    history.push(`/devices/edit/${id}`);
  };
  const handleDeleteAction = async (id: string) => {
    await api.deleteDevice(id, auth.user?.token || '');
    refetch();
  };
  const handleDetailsAction = (id: string) => {
    history.push(`/devices/details/${id}`);
  };

  const [{ data, loading, error }, refetch] = api.useGetDevices(
    auth.user?.token || ''
  );

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error!</p>;

  const { Data = [] } = data;
  return (
    <>
      <Link className='right' to='/devices/new'>
        Add New device
      </Link>
      <Table striped bordered hover>
        <thead>
          <th>Id</th>
          <th>Name</th>
          <th>Brand</th>
          <th>Actions</th>
        </thead>
        <tbody>
          {Data?.map((device: Device) => {
            return (
              <DeviceRow
                device={device}
                onEdit={handleEditAction}
                onDelete={handleDeleteAction}
                onDetails={handleDetailsAction}></DeviceRow>
            );
          })}
        </tbody>
      </Table>
    </>
  );
};
