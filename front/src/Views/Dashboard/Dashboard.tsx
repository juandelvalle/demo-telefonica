import React from 'react';
import { Container, Row, Col, Alert } from 'react-bootstrap';
import { WeatherService } from '../../Services/WeatherService';

export const Dashboard = () => {
  const api = new WeatherService();
  const [{ data, loading, error }, refetch] = api.useGetWeather('madrid');

  if (loading) {
    return <>Loading...</>;
  }
  if (error) {
    return (
      <Container>
        <Alert variant='danger'>Error: {error}</Alert>
      </Container>
    );
  }

  const { main } = data;

  return (
    <Container>
      <Row>Weather in Madrid:</Row>
      {main && (
        <Row>
          <Col>Temperature: {main.temp}º</Col>
          <Col>Temperature Min.: {main.temp_min}º</Col>
          <Col>Temperature Max.: {main.temp_max}º</Col>
        </Row>
      )}
    </Container>
  );
};
