import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Home } from './Views/Home/Home';
import PrivateRoute from './Components/PrivateRoute';
import { Login } from './Views/Login/Login';
import './App.css';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path='/login' exact component={Login} />
        <PrivateRoute path='/' component={Home} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
