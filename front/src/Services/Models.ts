export type LoginResponse = {
  status: string;
  token: string;
};

export type Brand = {
  _id?: string;
  name: string;
};

export type Device = {
  _id?: string;
  name: string;
  brand: Brand;
};
