import { Brand, Device, LoginResponse } from './Models';
import axios, { AxiosResponse } from 'axios';
import useAxios from 'axios-hooks';

// TODO: Inject this variable from container environment
const baseSite = 'http://localhost:8080';

export class ApiService {
  public async login(email: string, password: string) {
    const resp: AxiosResponse<LoginResponse> = await axios.post(
      `${baseSite}/auth/login`,
      {
        email,
        password,
      }
    );

    return resp.data;
  }

  public async createBrand(name: string, token: string) {
    const resp: AxiosResponse<LoginResponse> = await axios.post(
      `${baseSite}/brands`,
      {
        name,
      },
      {
        headers: {
          authorization: `Bearer ${token}`,
        },
      }
    );

    return resp.data;
  }

  public async updateBrand(brand: Brand, token: string) {
    const { _id, name } = brand;
    const resp: AxiosResponse<LoginResponse> = await axios.put(
      `${baseSite}/brands`,
      {
        _id,
        name,
      },
      {
        headers: {
          authorization: `Bearer ${token}`,
        },
      }
    );

    return resp.data;
  }

  public async deleteBrand(id: string, token: string) {
    const resp: AxiosResponse<LoginResponse> = await axios.delete(
      `${baseSite}/brands/${id}`,
      {
        headers: {
          authorization: `Bearer ${token}`,
        },
      }
    );

    return resp.data;
  }

  public async createDevice({ name, brand }: Device, token: string) {
    const resp: AxiosResponse<LoginResponse> = await axios.post(
      `${baseSite}/devices`,
      {
        name,
        brand,
      },
      {
        headers: {
          authorization: `Bearer ${token}`,
        },
      }
    );

    return resp.data;
  }

  public async updateDevice(device: Device, token: string) {
    const { _id, name, brand } = device;
    const resp: AxiosResponse<LoginResponse> = await axios.put(
      `${baseSite}/devices`,
      {
        _id,
        name,
        brand,
      },
      {
        headers: {
          authorization: `Bearer ${token}`,
        },
      }
    );

    return resp.data;
  }

  public async deleteDevice(id: string, token: string) {
    const resp: AxiosResponse<LoginResponse> = await axios.delete(
      `${baseSite}/devices/${id}`,
      {
        headers: {
          authorization: `Bearer ${token}`,
        },
      }
    );

    return resp.data;
  }

  // Effects
  useGetBrands(token: string) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    return useAxios(
      {
        url: `${baseSite}/brands`,
        headers: {
          authorization: `Bearer ${token}`,
        },
      },
      { useCache: false }
    );
  }

  useGetBrand(id: string, token: string) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    return useAxios(
      {
        url: `${baseSite}/brands/${id}`,
        headers: {
          authorization: `Bearer ${token}`,
        },
      },
      { useCache: false }
    );
  }

  useGetDevices(token: string) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    return useAxios(
      {
        url: `${baseSite}/devices`,
        headers: {
          authorization: `Bearer ${token}`,
        },
      },
      { useCache: false }
    );
  }

  useGetDevice(id: string, token: string) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    return useAxios(
      {
        url: `${baseSite}/devices/${id}`,
        headers: {
          authorization: `Bearer ${token}`,
        },
      },
      { useCache: false }
    );
  }
}
