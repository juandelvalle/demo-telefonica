import useAxios from 'axios-hooks';

// TODO: Inject these variables from container environment
const baseSite = 'https://api.openweathermap.org';
const apiKey = '1f0b663e96cd40c30e17bade88846f39';

export class WeatherService {
  useGetWeather(city: string) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    return useAxios(
      {
        url: `${baseSite}/data/2.5/weather?q=${city}&appid=${apiKey}&units=metric`,
      },
      { useCache: false }
    );
  }
}
