import React from 'react';
import { useDispatch } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { AuthLogoutAction } from '../Store/Auth/Actions';

export interface NavBarProps {
  user: {
    email: string;
    role: string;
  } | null;
}
export const NavBar = ({ user }: NavBarProps) => {
  const { role = '' } = user || { role: '' };
  const history = useHistory();
  const dispatch = useDispatch();

  const handleLogout = () => {
    localStorage.removeItem('dtauth');
    dispatch(AuthLogoutAction());
    history.push('/');
  };

  return (
    <nav id='sidebar'>
      <ul className='list-unstyled components'>
        <li>
          <Link to='/'>Home</Link>
        </li>
        <li>
          <Link to='/devices'>Devices</Link>
        </li>
        {role === 'admin' && (
          <li>
            <Link to='/brands'>Brands</Link>
          </li>
        )}
        <li>
          <Link to='/logout' onClick={handleLogout}>
            Logout
          </Link>
        </li>
      </ul>
    </nav>
  );
};
