import { Route, Redirect } from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import { useDispatch, useSelector } from 'react-redux';
import { AUTH_LOGIN_SUCCESS } from '../Store/Auth/Actions';
import { AppState } from '../Store';

const PrivateRoute = ({ component: Component, ...rest }: any) => {
  const dispatch = useDispatch();
  const auth = useSelector((state: AppState) => state.auth);
  const token = localStorage.getItem('dtauth');

  if (token && !auth.user) {
    // TODO: Validate token against an api endpoint to ensure it is correct
    const tokenDecoded: any = jwt_decode(token);
    dispatch({
      type: AUTH_LOGIN_SUCCESS,
      payload: { user: { ...tokenDecoded.user, token } },
    });
  }

  return (
    <Route
      {...rest}
      render={(props) =>
        !token ? <Redirect to='/login' /> : <Component {...props} />
      }
    />
  );
};

export default PrivateRoute;
