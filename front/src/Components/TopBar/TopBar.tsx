import React from 'react';

import './TopBar.css';

export interface TopBarProps {
  user: {
    email: string;
    role: string;
  } | null;
}
export const TopBar = ({ user }: TopBarProps) => {
  const { email, role } = user || { email: '', role: '' };

  return (
    <div className='topbar-header'>
      <h4>Technical demo</h4>
      <div className='login'>
        <div>{`Logged as ${email}(${role})`}</div>
      </div>
    </div>
  );
};
