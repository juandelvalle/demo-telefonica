import { Brand } from '../Services/Models';

export function getBrandNameFromId(id: string, brands: Brand[]): string {
  for (let i = 0; i < brands.length; i++) {
    if (brands[i]._id === id) {
      return brands[i].name;
    }
  }

  return '';
}
