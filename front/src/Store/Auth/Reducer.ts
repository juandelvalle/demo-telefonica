import {
  AUTH_LOGIN,
  AUTH_LOGIN_ERROR,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGOUT,
} from './Actions';

export type UserAuth = {
  token?: string;
  userId: string;
  email: string;
  role: string;
};

export type AuthState = {
  loggingInProgress: boolean;
  error: string;
  user: UserAuth | null;
};

const defaultAuthState: AuthState = {
  loggingInProgress: false,
  error: '',
  user: null,
};

export function AuthReducer(
  state: AuthState = defaultAuthState,
  action: any
): AuthState {
  switch (action.type) {
    case AUTH_LOGIN:
      return { ...state, loggingInProgress: true, error: '', user: null };
    case AUTH_LOGIN_SUCCESS:
      return {
        ...state,
        loggingInProgress: false,
        error: '',
        user: action.payload.user,
      };
    case AUTH_LOGIN_ERROR:
      return {
        ...state,
        loggingInProgress: false,
        error: action.payload.message,
        user: null,
      };
    case AUTH_LOGOUT:
      return {
        ...state,
        loggingInProgress: false,
        error: '',
        user: null,
      };

    default:
      return state;
  }
}
