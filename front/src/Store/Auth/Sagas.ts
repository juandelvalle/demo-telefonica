import jwt_decode from 'jwt-decode';
import { ApiService } from './../../Services/ApiService';
import { call, put, takeLatest } from 'redux-saga/effects';
import { AUTH_LOGIN_SUCCESS, AUTH_LOGIN_ERROR, AUTH_LOGIN } from './Actions';

function* loginRequest(action: any) {
  const Api = new ApiService();

  try {
    const user = yield call(
      Api.login,
      action.payload.email,
      action.payload.password
    );

    const tokenDecoded: any = jwt_decode(user.token);

    yield put({
      type: AUTH_LOGIN_SUCCESS,
      payload: { user: { ...tokenDecoded.user, token: user.token } },
    });
  } catch (e) {
    yield put({
      type: AUTH_LOGIN_ERROR,
      payload: { message: e.message || e.status },
    });
  }
}

export function* AuthSaga() {
  yield takeLatest(AUTH_LOGIN, loginRequest);
}
