export const AUTH_LOGIN = 'AUTH_LOGIN_REQUEST';
export const AUTH_LOGIN_SUCCESS = 'AUTH_LOGIN_SUCCESS';
export const AUTH_LOGIN_ERROR = 'AUTH_LOGIN_ERROR';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';

export type AuthLoginActionParams = {
  email: string;
  password: string;
};

export const AuthLoginAction = (loginData: AuthLoginActionParams) => {
  return { type: AUTH_LOGIN, payload: loginData };
};

export const AuthLogoutAction = () => {
  return { type: AUTH_LOGOUT };
};
