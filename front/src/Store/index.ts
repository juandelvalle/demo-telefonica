import { applyMiddleware, combineReducers, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { AuthSaga } from './Auth/Sagas';
import { AuthState, AuthReducer } from './Auth/Reducer';

export type AppState = {
  auth: AuthState;
};

const rootReducer = combineReducers({
  auth: AuthReducer,
});

const sagaMiddleware = createSagaMiddleware();
const middlewareEnhancer = applyMiddleware(sagaMiddleware);

const composedEnhancers = composeWithDevTools(middlewareEnhancer);

const store = createStore(rootReducer, {}, composedEnhancers);

export default store;

sagaMiddleware.run(AuthSaga);
