package middlewares

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"juandelvalle.com/dt-api/models"
	"net/http"
)

func IsAdmin(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		user := c.Get("user").(*jwt.Token)
		claims := user.Claims.(jwt.MapClaims)
		jwtUser := claims["user"].(map[string]interface {})

		if jwtUser["role"] != "admin" {
			return c.JSON(http.StatusUnauthorized, models.NewApiRequest("Need to be an admin", nil))
		}

		return next(c)
	}
}

func IsOperatorOrAdmin(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		user := c.Get("user").(*jwt.Token)
		claims := user.Claims.(jwt.MapClaims)
		jwtUser := claims["user"].(map[string]interface {})

		if jwtUser["role"] != "admin" && jwtUser["role"] != "operator" {
			return c.JSON(http.StatusUnauthorized, models.NewApiRequest("Need to be an operator", nil))
		}

		return next(c)
	}
}