package store

import (
	"fmt"
	"log"

	"juandelvalle.com/dt-api/models"
	"juandelvalle.com/dt-api/utils/jwt"
)

func addDemoData(st *Store) {
	password, err := jwt.HashPassword("demo")
	if err != nil {
		log.Fatal(err.Error())
	}

	_, err = st.FindUserByEmail("admin@demo.com")
	if err != nil {
		admin := models.User{
			Email:    "admin@demo.com",
			Password: password,
			Role:     "admin",
		}
		_, err := st.InsertUser(admin)
		if err != nil {
			fmt.Println(err)
		}
	}

	_, err = st.FindUserByEmail("operator@demo.com")
	if err != nil {
		operator := models.User{
			Email:    "operator@demo.com",
			Password: password,
			Role:     "operator",
		}
		_, err := st.InsertUser(operator)
		if err != nil {
			fmt.Println(err)
		}
	}
}
