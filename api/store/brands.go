package store

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"juandelvalle.com/dt-api/models"
)

func (st *Store) GetAllBrands() ([]models.Brand, error) {
	var brands []models.Brand
	cursor, err := st.brandsCollection.Find(context.TODO(), bson.D{})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(context.TODO())

	for cursor.Next(context.TODO()) {
		var elem models.Brand
		err := cursor.Decode(&elem)
		if err != nil {
			return nil, err
		}

		brands = append(brands, elem)
	}
	if err := cursor.Err(); err != nil {
		return nil, err
	}

	return brands, err
}

func (st *Store) FindBrandById(brandId string) (models.Brand, error) {
	brand := models.Brand{}

	filter := bson.M{"_id":  brandId}
	err := st.brandsCollection.FindOne(context.TODO(), filter).Decode(&brand)

	return brand, err
}


func (st *Store) InsertBrand(brand models.Brand) (string, error) {
	brand.ID = st.GetNewId()
	result, err := st.brandsCollection.InsertOne(context.TODO(), brand)
	if err != nil {
		return "", err
	}

	return result.InsertedID.(string), nil
}

func (st *Store) UpdateBrand(brand models.Brand) error {
	if brand.ID == "" {
		return fmt.Errorf("error updating card")
	}

	filter := bson.M{"_id":  brand.ID}
	_, err := st.brandsCollection.ReplaceOne(context.TODO(), filter, brand)
	return err
}


func (st *Store) DeleteBrand(brandId string) error {
	filter := bson.M{"_id":  brandId}
	_, err := st.brandsCollection.DeleteOne(context.TODO(), filter)
	return err
}

