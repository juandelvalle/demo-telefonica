package store

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"juandelvalle.com/dt-api/models"
)

func (st *Store) GetAllDevices() ([]models.Device, error) {
	var devices []models.Device
	cursor, err := st.devicesCollection.Find(context.TODO(), bson.D{})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(context.TODO())

	for cursor.Next(context.TODO()) {
		var elem models.Device
		err := cursor.Decode(&elem)
		if err != nil {
			return nil, err
		}

		devices = append(devices, elem)
	}
	if err := cursor.Err(); err != nil {
		return nil, err
	}

	return devices, err
}

func (st *Store) FindDeviceById(deviceId string) (models.Device, error) {
	device := models.Device{}

	filter := bson.M{"_id":  deviceId}
	err := st.devicesCollection.FindOne(context.TODO(), filter).Decode(&device)

	return device, err
}


func (st *Store) InsertDevice(device models.Device) (string, error) {
	device.ID = st.GetNewId()
	result, err := st.devicesCollection.InsertOne(context.TODO(), device)
	if err != nil {
		return "", err
	}

	return result.InsertedID.(string), nil
}

func (st *Store) UpdateDevice(device models.Device) error {
	if device.ID == "" {
		return fmt.Errorf("error updating card")
	}

	filter := bson.M{"_id":  device.ID}
	_, err := st.devicesCollection.ReplaceOne(context.TODO(), filter, device)
	return err
}


func (st *Store) DeleteDevice(deviceId string) error {
	filter := bson.M{"_id":  deviceId}
	_, err := st.devicesCollection.DeleteOne(context.TODO(), filter)
	return err
}

