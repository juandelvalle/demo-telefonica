package store

import (
	"context"

	"juandelvalle.com/dt-api/models"

	"go.mongodb.org/mongo-driver/bson"
)

func (st *Store) FindUserByEmail(email string) (models.User, error) {
	user := models.User{}
	filter := bson.M{"email": email}
	err := st.usersCollection.FindOne(context.TODO(), filter).Decode(&user)
	return user, err
}

func (st *Store) InsertUser(user models.User) (string, error) {
	user.ID = st.GetNewId()
	insertResult, err := st.usersCollection.InsertOne(context.TODO(), user)
	if err != nil {
		return "", err
	}

	return insertResult.InsertedID.(string), err
}
