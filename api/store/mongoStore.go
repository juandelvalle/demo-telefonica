package store

import (
	"context"
	"log"
	"sync"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Store struct {
	mongoDB         string
	mongoUrl        string
	mongoDBClient   *mongo.Client
	usersCollection *mongo.Collection
	brandsCollection *mongo.Collection
	devicesCollection *mongo.Collection
}

var mux sync.Mutex
var instance *Store = nil

func GetStore() *Store {
	mux.Lock()
	defer mux.Unlock()

	if instance != nil {
		return instance
	}

	instance = &Store{}
	instance.init()

	return instance
}

func (st *Store) init() {
	// TODO: Get these variables from evironment
	st.mongoDB = "demodt"
	st.mongoUrl = "mongodb://jdelvalle:mongopwd@mongo:27017/?authSource=admin"

	clientOptions := options.Client().ApplyURI(st.mongoUrl)
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}

	st.mongoDBClient = client
	st.usersCollection = client.Database(st.mongoDB).Collection("users")
	st.brandsCollection = client.Database(st.mongoDB).Collection("brands")
	st.devicesCollection = client.Database(st.mongoDB).Collection("devices")
	// TODO: Create unique indexes for collections

	// Adding demo data
	addDemoData(st)
}

func (*Store) GetNewId() string {
	return primitive.NewObjectID().Hex()
}
