package models


type Device struct {
	ID       string `bson:"_id" json:"_id"`
	Name string`bson:"name" json:"name"`
	Brand Brand `bson:"brand" json:"brand"`
}

