package models

type APIResponse struct {
	Error string
	Data interface {}
}

func NewApiRequest(error string, data interface {}) APIResponse {
	return APIResponse { Error: error, Data: data}
}