package models

type User struct {
	ID       string `bson:"_id" json:"_id"`
	Email    string `bson:"email" json:"email"`
	Password string `bson:"password" json:"password"`
	Role     string `bson:"role" json:"role"`
}
