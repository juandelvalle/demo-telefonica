package jwt

import (
	"fmt"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"juandelvalle.com/dt-api/models"
)

// TODO: Get this variable from environment
const JWT_SECRET = "kjehvnewjrkhn"


type JWTClaims struct {
	jwt.StandardClaims
	User JWTUser `json:"user"`
}

type JWTUser struct {
	Id string `json:"id"`
	Email string  `json:"email"`
	Role string `json:"role"`
}

func GenerateToken(dbuser *models.User) (string, error) {
	st := time.Now().Add(time.Hour * 72).Unix()
	n := time.Now().Unix()
	claims := JWTClaims{
		jwt.StandardClaims{
			ExpiresAt: st,
			IssuedAt:  n,
			Issuer:    "https://juandelvalle.com",
			NotBefore: n,
		},
		JWTUser{
			Id:    dbuser.ID,
			Email: dbuser.Email,
			Role:  dbuser.Role,
		},
	}

	// Create token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	t, err := token.SignedString([]byte(JWT_SECRET))
	if err != nil {
		return "", err
	}

	return t, nil
}

func ParseToken(tokenString string) (*models.User, error) {
	var user models.User

	if strings.Contains(tokenString, "Bearer ") {
		tokenString = strings.Replace(tokenString, "Bearer ", "", -1)
	}

	token, err := jwt.ParseWithClaims(tokenString, &JWTClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(JWT_SECRET), nil
	})
	if err != nil {
		return nil, err
	}

	if claims, ok := token.Claims.(*JWTClaims); ok && token.Valid {
		fmt.Printf("%v %v", claims.User.Id, claims.StandardClaims.ExpiresAt)
		user.ID = claims.User.Id
		user.Email = claims.User.Email

		return &user, nil
	}

	return nil, err
}

// Maybe there is a better place to put these utility functions
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
