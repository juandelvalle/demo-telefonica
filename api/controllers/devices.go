package controllers

import (
	"github.com/labstack/echo/v4"
	"juandelvalle.com/dt-api/models"
	"juandelvalle.com/dt-api/store"
	"net/http"
)

func ListDevices(c echo.Context) error {
	store := store.GetStore()
	devices, err := store.GetAllDevices()
	if err != nil {
		return c.JSON(http.StatusNotFound, models.NewApiRequest(err.Error(), nil))
	}

	return c.JSON(http.StatusOK, models.NewApiRequest("", devices))
}

func GetDevice(c echo.Context) error {
	store := store.GetStore()
	deviceId := c.Param("id")

	device, err := store.FindDeviceById(deviceId)
	if err != nil {
		return c.JSON(http.StatusNotFound, models.NewApiRequest(err.Error(), nil))
	}

	return c.JSON(http.StatusOK, models.NewApiRequest("", device))
}

func AddDevice(c echo.Context) error {
	store := store.GetStore()
	request := new(models.Device)
	if err := c.Bind(request); err != nil {
		return c.JSON(http.StatusBadRequest, models.NewApiRequest(err.Error(), nil))
	}

	newId, err := store.InsertDevice(*request)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.NewApiRequest(err.Error(), nil))
	}

	request.ID = newId
	return c.JSON(http.StatusCreated, models.NewApiRequest("", request))
}

func UpdateDevice(c echo.Context) error {
	store := store.GetStore()
	request := new(models.Device)
	if err := c.Bind(request); err != nil {
		return c.JSON(http.StatusBadRequest, models.NewApiRequest(err.Error(), nil))
	}

	err := store.UpdateDevice(*request)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.NewApiRequest(err.Error(), nil))
	}

	return c.JSON(http.StatusOK, models.NewApiRequest("", request))
}

func DeleteDevice(c echo.Context) error {
	store := store.GetStore()
	deviceId := c.Param("id")

	err := store.DeleteDevice(deviceId)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.NewApiRequest(err.Error(), nil))
	}

	return c.NoContent(http.StatusOK)
}