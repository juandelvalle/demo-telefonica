package controllers

import (
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
	"juandelvalle.com/dt-api/models"
	"juandelvalle.com/dt-api/store"
	"juandelvalle.com/dt-api/utils/jwt"
)

func Login(c echo.Context) error {
	store := store.GetStore()
	response := models.LoginResponse{
		Status: "",
		Token:  "",
	}

	request := new(models.LoginRequest)
	if err := c.Bind(request); err != nil {
		return c.JSON(http.StatusBadRequest, response)
	}

	dbuser, err := store.FindUserByEmail(strings.ToLower(request.Email))
	if err != nil {
		response.Status = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	p := request.Password
	h := dbuser.Password
	checked := jwt.CheckPasswordHash(p, h)
	if checked == false {
		response.Status = "Invalid password"
		return c.JSON(http.StatusForbidden, response)
	}

	t, err := jwt.GenerateToken(&dbuser)
	if err != nil {
		response.Status = err.Error()
		return c.JSON(http.StatusInternalServerError, response)
	}

	response.Token = t
	response.Status = "OK"
	return c.JSON(http.StatusOK, response)
}

func Logout(c echo.Context) error {
	// The rest api actually has noting to do for a user to logout
	return c.NoContent(http.StatusOK)
}
