package controllers

import (
	"github.com/labstack/echo/v4"
	"juandelvalle.com/dt-api/models"
	"juandelvalle.com/dt-api/store"
	"net/http"
)

func ListBrands(c echo.Context) error {
	store := store.GetStore()
	brands, err := store.GetAllBrands()
	if err != nil {
		return c.JSON(http.StatusNotFound, models.NewApiRequest(err.Error(), nil))
	}

	return c.JSON(http.StatusOK, models.NewApiRequest("", brands))
}

func GetBrand(c echo.Context) error {
	store := store.GetStore()
	brandId := c.Param("id")

	brand, err := store.FindBrandById(brandId)
	if err != nil {
		return c.JSON(http.StatusNotFound, models.NewApiRequest(err.Error(), nil))
	}

	return c.JSON(http.StatusOK, models.NewApiRequest("", brand))
}

func AddBrand(c echo.Context) error {
	store := store.GetStore()
	request := new(models.Brand)
	if err := c.Bind(request); err != nil {
		return c.JSON(http.StatusBadRequest, models.NewApiRequest(err.Error(), nil))
	}

	newId, err := store.InsertBrand(*request)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.NewApiRequest(err.Error(), nil))
	}

	request.ID = newId
	return c.JSON(http.StatusCreated, models.NewApiRequest("", request))
}

func UpdateBrand(c echo.Context) error {
	store := store.GetStore()
	request := new(models.Brand)
	if err := c.Bind(request); err != nil {
		return c.JSON(http.StatusBadRequest, models.NewApiRequest(err.Error(), nil))
	}

	err := store.UpdateBrand(*request)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.NewApiRequest(err.Error(), nil))
	}

	return c.JSON(http.StatusOK, models.NewApiRequest("", request))
}

func DeleteBrand(c echo.Context) error {
	store := store.GetStore()
	brandId := c.Param("id")

	err := store.DeleteBrand(brandId)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.NewApiRequest(err.Error(), nil))
	}

	return c.NoContent(http.StatusOK)
}

