package main

import "juandelvalle.com/dt-api/store"

func main() {

	// Init Database
	store.GetStore()

	server := NewApiServer()

	// TODO. Get port from environment
	server.StartServer(":8080")
}
