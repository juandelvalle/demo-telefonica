package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"juandelvalle.com/dt-api/controllers"
	"juandelvalle.com/dt-api/middlewares"
)

// TODO: Get this variable from environment
const JWT_SECRET = "kjehvnewjrkhn"

type Server struct {
	echoServer *echo.Echo
}

func NewApiServer() *Server {
	e := echo.New()

	e.Use(middleware.Logger()) // Logger
	e.Use(middleware.Recover()) // Recover
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH,    echo.POST, echo.DELETE},
	}))

	createAuthGroup(e)
	createDeviceGroup(e)
	createBrandsGroup(e)

	return &Server{
		echoServer: e,
	}
}

func (server *Server) StartServer(port string) {

	server.echoServer.Logger.Fatal(server.echoServer.Start(port))
}

func createAuthGroup(e *echo.Echo) *echo.Group {
	g := e.Group("/auth")

	g.POST("/login", controllers.Login)
	g.POST("/logout", controllers.Logout)

	return g
}

func createDeviceGroup(e *echo.Echo) *echo.Group {
	g := e.Group("/devices")
	g.Use(middleware.JWT([]byte(JWT_SECRET)), middlewares.IsOperatorOrAdmin)

	// List all devices
	g.GET("", controllers.ListDevices)

	// Get a devices
	g.GET("/:id", controllers.GetDevice)

	// Insert a device
	g.POST("", controllers.AddDevice)

	// Update a device
	g.PUT("", controllers.UpdateDevice)

	// Delete a device
	g.DELETE("/:id", controllers.DeleteDevice)

	return g
}

func createBrandsGroup(e *echo.Echo) *echo.Group {
	g := e.Group("/brands")
	g.Use(middleware.JWT([]byte(JWT_SECRET)))

	// List all brands
	g.GET("", controllers.ListBrands, middlewares.IsOperatorOrAdmin)

	// Get a brand
	g.GET("/:id", controllers.GetBrand, middlewares.IsOperatorOrAdmin)

	// Insert a brand
	g.POST("", controllers.AddBrand, middlewares.IsAdmin)

	// Update a brand
	g.PUT("", controllers.UpdateBrand, middlewares.IsAdmin)

	// Delete a brand
	g.DELETE("/:id", controllers.DeleteBrand, middlewares.IsAdmin)

	return g
}
