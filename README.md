# Prueba Telefonica

Prueba técnica para telefónica


# Puesta en marcha
Todo el proyecto se encuentra contenerizado mediante docker y se incluye un ficher docker-compose listo para lanzarlo.

Para ejecutar el proyecto completo, en un terminal ejecutar en el directorio raíz del proyecto
```
docker-compose up -d
```

Una vez arrancados, abrir un navegador e ir a la dirección
```
    http://localhost:3000
```

# Usuarios de prueba
Al arrancar el backend se generan dos usuarios de prueba:
- admin@demo.com : demo
- operator@demo.com : demo 


# Estructura del proyecto
Todo el proyecto se encuentra en un único repositorio de gitlab.
El el subdirectorio __api__ se encuentra el backend, que se trata de un proyecto en Go, empleando la librería Echo para implementar el server.
En el subdirectorio __front__ se encuentra la web, realizada mediante React y Boostrap y con Typescript. 

A lo largo del código se encuentran varios comentarios TODO, con cosas que se podrían mejorar pero no se ha hecho por falta de tiempo.

Para el desarrollo del proyecto, se ha empleado la metodología GitFlow. Encontrándose la versión final del desarrollo en la rama **master**.

# Consideraciones Front
Como prueba técnica, se han empleado varios tipos de componentes React, para ilustrar tanto el uso de componentes declarados como clase, y declarados como funciones para mostrar el uso de hooks.
Debido al escaso tiempo de la prueba, no he trabajado demasiado en el diseño de la interfaz ni la maquetación.
Para demostrar el uso de Redux, se ha implementado el estado de autenticación del usuario, que es global a toda la aplicación. Además, se emplean sagas para trabajar la asincronía de la llamada al api de login.
Para los datos locales a cada pantalla, se ha optado por no guardarlos en Redux, y en su lugar emplear la los hooks de axios, librería utilizada para acceder al backend.

# Base de datos
La base de datos empleada es Mongo.
Las colecciones se generan la primera vez que se arranca el backend.
La bbdd consta de las siguientes colecciones:
- *users* - Contiene los usuarios que se pueden logar en la aplicación. Como se comenta anteriormente, la primera vez que arranca el proyecto se genera un usuario de cada tipo.
- *brands* - Contiene los datos de las marcas. Simplemente un nombre con la marca.
- *devices* - Contiene los datos de los móviles. Se ha decidido a guardar la marca completa junto con el móvil, para facilitar el tratamiento de las marcas eliminadas. En un proyecto real, se debería optar por alguna otra estrategia, como marcar las marcas como eliminadas, sin borrar el documento de la colección.
